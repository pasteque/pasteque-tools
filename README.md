# Pasteque tools

Just a few tools.

user_ini.php
============
A php page and script to create <user>_id.ini files.

backup_ini.php
========
Generates an sh script to dump database from your *_db.ini files

htmljs
======
Contains various web-based tools including PastequeJS lib. All of these may require you to enable CORS on your targeted server.

lib/*
-----
Contains the core files for PastequeJS, shared accross all the following tools.

monitor.*
---------
A simple monitoring tool to check if your installations are up.

Set your monitored instances in monitor-config.js and see the requests fly (or not).

viewer.*
--------
A simple remote display of orders (shared tickets).

Put the connection credentials in viewer-config.js and open viewer.html to see the pending orders.

order.*
-------
A remote order taking tool. Edit orders that will be passed to an other client to proceed to the checkout.

Edit order-config.js with the server connection data (use a dedicated Cash Register name)

pasteque-backup
===============
Utility for Backup MySQL and PostgreSQL databases daily and monthly with automatic deletion of the old backups.
There are no more than 19 backups in the backup folder, 7 daily plus 12 monthly. 
Use cron service to be launched.
Use external commands: mysql, mysqldump, psql, pg_dump.
Tested with rclone and grive to save backups in a safe place.
