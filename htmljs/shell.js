var send = function() {
    var srv = Pasteque.Connection(
        document.getElementById('server').value,
        document.getElementById('user').value,
        document.getElementById('password').value);
    var json = document.getElementById('params').value;
    if (json == '') { json = '{}'; }
    var jsargs = {};
    try {
        jsargs = JSON.parse(json);
    } catch (e) {
        callback("Param error: " + e.message);
        return false;
    }
    var method = document.getElementById('method').value;
    var target = document.getElementById('target').value;
    var reqArgs = [target];
    for (var key in jsargs) {
        reqArgs.push(key); args.push(jsargs[key]);
    }
    var request = Pasteque.Request.apply(null, reqArgs);
    if (method == 'GET') {
        Pasteque.srv_read(srv, request, successCallback, errorCallback);
    } else {
        Pasteque.srv_write(srv, request, successCallback, errorCallback);
    }
    return false;
}
var successCallback = function(data) {
    var div = document.getElementById('response');
    div.innerHTML = '';
    div.appendChild(document.createTextNode(data));
}
var errorCallback = function(request, status, text) {
    var div = document.getElementById('response');
    div.innerHTML = '';
    var data = 'status: ' + status + "\n" + text;
    div.appendChild(document.createTextNode(data));
}
