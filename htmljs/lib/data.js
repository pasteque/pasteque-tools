/* Pasteque JSLib
 *
 * This file is part of Pasteque JSLib
 *
 * Pasteque JSLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pasteque JSLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Main controller and data keeper for Pasteque applications
// This is probably shitty code to use
// untill a good framework is defined.

var Pasteque = (function(mod, status) {

    /** Initialize an application with a given connection.
     * Pass this object to every app functions. */
    mod.AppData = function(conn, cashRegisterName) {
        return {
            "conn": conn,
            "cashRegisterName": cashRegisterName,
            "catalog": null,
            "floors": null,
        }
    };

    /** Load the catalog into the app.
     * @param app {AppData} Application object.
     * @param callback {function} Function called on succes
     * without arguments.
     * @param error {function} Function called on error
     * with (request, status, message) arguments. */
    mod.sync = function(app, callback, error) {
        var syncDoneClosure = function(app, callback, errCallback) {
            return function(data) { syncDone(data, app, callback, errCallback); }
        }
        var errorClosure = function(errCallback) {
            return function(msg) { errCallback(null, '200', msg); }
        }
        Pasteque.srv_read(app.conn, Pasteque.Request('SyncAPI', 'sync', 'cashregister', app.cashRegisterName), syncDoneClosure(app, callback, errorClosure(error)), error);
    };

    /** Internal sync callback.
     * @param callback {function} callback().
     * @param errorCallback {function} callback(message). */
    function syncDone(data, app, callback, errorCallback) {
        var status = data['status'];
        if (status != 'ok') { errorCallback('Sync call status nok: ' + status + ' - ' + JSON.stringify(data['content'])); return; }
        var content = data['content'];
        // Get taxes
        var taxes = content['taxes'];
        var nowSec = new Date().getTime() / 1000;
        for (var i = 0; i < taxes.length; i++) {
            var taxCat = taxes[i];
            var maxDate = 0;
            var index = 0;
            for (var j = 0; j < taxCat.length; j++) {
                var tax = taxCat.taxes[j];
                if (tax.validFrom < nowSec && tax.validFrom > maxDate) {
                    maxDate = tax.validFrom;
                    index = j;
                }
            }
            taxCat.currentRate = taxCat.taxes[index].rate;
            taxCat.currentTaxId = taxCat.taxes[index].id;
        }
        app.taxes = taxes
        // Get categories
        var categories = content['categories'];
        // Get produts
        var products = [];
        var jsProducts = content['products'];
        for (var i = 0; i < jsProducts.length; i++) {
            var prd = jsProducts[i];
            if (!prd['visible']) { continue; }
            products.push(prd);
        }
        // Get compositions
        
        app.catalog = new Pasteque.Catalog(categories, products);
        // Get places
        app.floors = content['floors'];
        callback();
    }

    return mod;
}(Pasteque || {}));
