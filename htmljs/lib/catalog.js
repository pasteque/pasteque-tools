/* Pasteque JSLib
 *
 * This file is part of Pasteque JSLib
 *
 * Pasteque JSLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pasteque JSLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var Pasteque = (function(mod, status) {

    mod.Catalog = function(categories, products) {
        var allCats = [];
        var categoryById = {};
        for (var i = 0; i < categories.length; i++) {
            var cat = categories[i];
            if (cat['parentId'] == null) { cat['parentId'] = ''; }
            if (!(cat['parentId'] in categoryById)) { categoryById[cat['parentId']] = []; }
            categoryById[cat['parentId']].push(cat);
            allCats.push(cat);
        }
        var productById = {};
        var productByCat = {};
        for (var i = 0; i < products.length; i++) {
            var prd = products[i];
            if (!prd['visible']) { continue; }
            if (!(prd['categoryId'] in productByCat)) { productByCat[prd['categoryId']] = []; }
            productById[prd['id']] = prd;
            productByCat[prd['categoryId']].push(prd);
        }
        this.categories = allCats;
        this.categoryById = categoryById;
        this.productById = productById;
        this.productByCat = productByCat;
    }

    mod.Catalog.prototype.getProduct = function(prdId) {
        return (prdId in this.productById) ? this.productById[prdId] : null;
    }
    mod.Catalog.prototype.getCategories = function(parentId) {
        if (parentId == null) { return this.categoryById['']; }
        else if (parentId in this.categoryById) {
            return this.categoryById(parentId);
        } else {
            return [];
        }
    }
    mod.Catalog.prototype.getProductsInCat = function(catId) {
        return (catId in this.productByCat) ? this.productByCat[catId] : [];
    }

    // Catalog widget

    /** Create and display a catalog widget.
     * @param conn {Connection} Pasteque connection for getting images.
     * @param containerId {string} Id of element to display catalog in
     * without #.
     * @param catalog {object} Catalog tree.
     * @param pickCallback {string} Name of the function called on click.
     * It takes the product id as argument. */
    mod.CatalogWidget = function(containerId, catalog, pickCallback) {
        var container = document.getElementById(containerId);
        if (!container) {
            console.error(containerId + ' not found in DOM for catalog widget');
            return;
        }
        var html = "<div class=\"catalog-categories-container\">"
        var rootCats = catalog.getCategories(null);
        for (var i = 0; i < rootCats.length; i++) {
            html += catHtml(containerId, rootCats[i]);
        }
        html += "</div>";
        for (var i = 0; i < catalog.categories.length; i++) {
            var cat = catalog.categories[i];
            var subCats = catalog.getCategories(cat['id']);
            var prds = catalog.getProductsInCat(cat['id']);
            html += "<div class=\"catalog-products-container hidden\" id=\"catalog-" + containerId + "-products-container-" + cat['id'] + "\">";
            for (var j = 0; j < subCats.length; j++) html += catHtml(containerId, subCats[j]);
            for (var j = 0; j < prds.length; j++) html += prdHtml(prds[j], pickCallback);
            html += "</div>";
        }
        container.innerHTML = html;
        // Select first top category
        document.getElementById('catalog-' + containerId + '-products-container-' + rootCats[0]['id']).classList.remove('hidden');
        container.dataset.selectedCat = rootCats[0]['id'];
        
    };

    /** Set class for label element according to it's length
     * for diminishing font size for example. */
    function getLabelClass(label) {
        var className = "catalog-label";
        if (label.length > 16) {
            className += " catalog-label-verylong";
        } else if (label.length > 8) {
            className += " catalog-label-long";
        }
        return className;
    }

    /** Subwidget for a category */
    function catHtml(catalogId, category) {
        var img = (category['hasImage']) ? " style=\"background-image: url(data:image/jpg;base64," + category['image'] + ");\"" : 'style="background-image : url(res/category_default.png);"';
        var html = "\t<div class=\"category-" + category.id + " catalog-category " + className + "\" onClick=\"javascript:Pasteque.changeCategory('" + catalogId + "', '" + category.id + "');return false;\"" + img + ">";
        var className = getLabelClass(category.label);
        html += "<p class=\"" + className + "\">" + category.label + "</p></div>";
        return html;
    }

    /** Subwidget for product. */
    function prdHtml(product, pickCallback) {
        var img = (product['hasImage']) ? " style=\"background-image: url(data:image/jpg;base64," + product['image'] + ")\"" : 'style="background-image : url(res/prd_default.png);"';
        html = "<div id=\"product-" + product['id'] + "\" class=\"catalog-product\" onClick=\"javascript:" + pickCallback + "('" + product['id'] + "');\"" + img + ">";
        var src;
        var className = getLabelClass(product['label']);
        html += "<p class=\"" + className + "\">" + product['label'] + "</p>";
        html += "</div>";
        return html;
    }

    /** Callback for clicking on a category. */
    mod.changeCategory = function(catalogId, categoryId) {
        var widgetElem = document.getElementById(catalogId);
        if (!widgetElem) { console.error('Cannot select catalog ' + catalogId); return; }
        var selected = widgetElem.dataset.selectedCat;
        document.getElementById('catalog-' + catalogId + '-products-container-' + selected).classList.add('hidden');
        document.getElementById('catalog-' + catalogId + '-products-container-' + categoryId).classList.remove('hidden');
        widgetElem.dataset.selectedCat = categoryId;
    }
    
    return mod;
}(Pasteque || {}));
