/* Pasteque JSLib
 *
 * This file is part of Pasteque JSLib
 *
 * Pasteque JSLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pasteque JSLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var Pasteque = (function(mod, status) {

    mod.placelistWidget = function(containerId, places, orders, pickCallback) {
        var container = document.getElementById(containerId);
        if (!container) {
            console.error(containerId + ' not found in DOM for places widget');
            return;
        }
        var html = "<ul class=\"placelist-container\">"
        for (var i = 0; i < places.length; i++) {
            var place = places[i];
            var free = '';
            for (var j = 0; j < orders.length; j++) {
                if (orders[j].id == place.id) {
                    free = ' placelist-place-occupied';;
                    break;
                }
            }
            html += "<li class=\"placelist-place" + free + "\" id=\"placelist-place-" + place['id'] + "\" onclick=\"javascript:" + pickCallback + "(" + place['id'] + ");\">" + place['label'] + "</li>";
        }
        html += "</ul>";
        container.innerHTML = html;
    }

    return mod;
}(Pasteque || {}));
