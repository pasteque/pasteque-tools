/* Pasteque JSLib
 *
 * This file is part of Pasteque JSLib
 *
 * Pasteque JSLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pasteque JSLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var Pasteque = (function(mod, status) {

    mod.orderContentWidget = function (containerId, order) {
        var container = document.getElementById(containerId);
        if (!container) {
            console.error(containerId + ' not found in DOM for order widget');
            return;
        }
        html += "<ul class=\"orderlines\">";
        for (var j = 0; j < order.lines.length; j++) {
            var line = order.lines[j];
            html += "<li>" + line.quantity + " " + products[line.productId] + "</li>"
        }
        html += "</ul>"
        container.innerHTML = html;
    }

    return mod;
}(Pasteque || {}));
