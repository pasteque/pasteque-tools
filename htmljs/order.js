var app = Pasteque.AppData(Pasteque.Connection(cfg['host'], cfg['user'], cfg['password']), cfg['cashregister']);
var currentOrder = null;
var screens = ['restaurant-screen', 'order-screen', 'confirm-screen'];
var refreshInterval = null;
function start() {
    Pasteque.sync(app, syncCallback, errCallback);
    showScreen('restaurant-screen');
}
function showLoading(loading) {
    if (loading) {
        document.getElementById('loading').classList.remove('hidden');
    } else {
        document.getElementById('loading').classList.add('hidden');
    }
}
function showScreen(screen) {
    if (refreshInterval != null) {
        clearInterval(refreshInterval);
    }
    for (var i = 0; i < screens.length; i++) {
        document.getElementById(screens[i]).classList.add('hidden');
    }
    document.getElementById(screen).classList.remove('hidden');
    switch (screen) {
    case 'restaurant-screen':
        showLoading(true);
        refreshOrders();
        refreshInterval = setInterval(refreshOrders, 5000);
        break;
    case 'confirm-screen':
        updateOrder(currentOrder);
        break;
    }
}
function updateOrderScreen(order) {
    updateOrderCustCount(order);
    updateOrderQty(order);
}
function updateOrderQty(order) {
    var qty = 0;
    for (var i = 0; i < order.lines.length; i++) {
        qty += order.lines[i].quantity;
    }
    document.getElementById('order-line-count').innerHTML = qty + ' article(s)';
}
function updateOrderCustCount(order) {
    document.getElementById('order-plate-count').innerHTML = (order.custCount == null) ? '.' : order.custCount + ' pers.';
}
function updateOrder(order) {
    document.getElementById('order-label').innerHTML = (order.custCount != null) ? order.label + ' (' + order.custCount + ' personnes)' : order.label;
    var htmlLines = '';
    for (var i = 0; i < order.lines.length; i++) {
        var prd = app.catalog.getProduct(order.lines[i].productId);
        if (prd == null) {
            logError('Product ' + order.lines[i].productId + ' not found in local data (order display)');
            continue;
        }
        htmlLines += "<li id=\"order-line-" + i + "\"><button class=\"btn btn-default\" onclick=\"javascript:lineMinus(" + i + ");\">-</button> <span id=\"order-line-" + i + "-qty\" class=\"order-line-quantity\">" + order.lines[i].quantity + "</span> <button class=\"btn btn-default\" onclick=\"javascript:linePlus(" + i + ");\">+</button> <span class=\"order-line-product\">" + prd.label + "</span></li>";
    }
    document.getElementById('order-content').innerHTML = htmlLines;
}
function syncCallback() {
    Pasteque.CatalogWidget('catalog', app.catalog, 'pickProduct');
    refreshOrders();
}
function refreshOrders() {
    Pasteque.srv_read(app.conn, Pasteque.Request('TicketsAPI', 'getAllShared'), orderCallback, errCallback);
}
function selectOrder(order) {
    currentOrder = order;
}
// Orders refresh functions
function orderCallback(data) {
    if (app.floors == null) { return; } // sync not done
    showLoading(false);
    app.orders = data['content'];
    Pasteque.placelistWidget('places', app.floors[0]['places'], app.orders, 'pickPlace');
}
function errCallback(req, status, msg) {
    showLoading(false);
    logError(msg);
}
function validateCallback(data) {
    if (data['content'] != true) {
        logError('Cannot save order: ' + data['content']);
    }
    showLoading(true);
    refreshOrders();
}
// UI callbacks
function pickPlace(placeId) {
    var place = null;
    currentOrder = null; // Reset just in case
    for (var i = 0; i < app.floors[0].places.length; i++) {
        if (app.floors[0].places[i].id == placeId) {
            place = app.floors[0].places[i];
            break;
        }
    }
    if (place == null) {
        logError('Place ' + placeId + ' not found in local data');
        return;
    }
    for (var i = 0; i < app.orders.length; i++) {
        if (app.orders[i].id == place.id) {
            currentOrder = app.orders[i];
            break;
        }
    }
    if (currentOrder == null) {
        currentOrder = {"id": placeId, "label": place['label'], "customerId": null, "custCount": null,
                        "tariffAreaId": null, "discountProfileId": null, "discountRate": null, "lines": []};
    }
    updateOrderScreen(currentOrder);
    showScreen('order-screen');
}
function pickProduct(prdId) {
    var prd = app.catalog.getProduct(prdId);
    if (prd == null) {
        logError('Product ' + prdId + ' not found in local data');
        return;
    }
    var lineFound = false;
    for (var i = 0; i < currentOrder.lines.length; i++) {
        var line = currentOrder.lines[i];
        if (line.productId == prdId) {
            line.quantity++;
            lineFound = true;
            break;
        }
    }
    if (!lineFound) {
        var taxId = null;
        for (var i = 0; i < app.taxes.length; i++) {
            if (prd.taxCatId == app.taxes[i].id) {
                taxId = app.taxes[i].currentTaxId;
            }
        }
        if (taxId == null) {
            logError ('Tax not found for product ' + prd.label + ', taxCat ' + prd.taxCatId);
            return;
        }
        currentOrder.lines.push({"sharedTicketId": currentOrder.id, "dispOrder": currentOrder.lines.length + 1, "productId": prdId, "taxId": taxId, "quantity": 1, "discountRate": null, "price": prd.priceSell, "attributes": null});
    }
    updateOrderQty(currentOrder);
}
function custCountPlus() {
    switch (currentOrder.custCount) {
    case null: currentOrder.custCount = 1; break;
    default: currentOrder.custCount ++; break;
    }
    updateOrderCustCount(currentOrder);
}
function custCountMinus() {
    switch (currentOrder.custCount) {
    case null: break;
    case 1: currentOrder.custCount = null; break;
    default: currentOrder.custCount --; break;
    }
    updateOrderCustCount(currentOrder);
}
function linePlus(lineNum) {
    var line = currentOrder.lines[lineNum];
    line.quantity++;
    document.getElementById('order-line-' + lineNum + '-qty').innerHTML = line.quantity;
}
function lineMinus(lineNum) {
    var line = currentOrder.lines[lineNum];
    if (line.quantity > 0) {
        line.quantity--;
    }
    document.getElementById('order-line-' + lineNum + '-qty').innerHTML = line.quantity;
}
function backOrder() {
    updateOrderScreen(currentOrder);
    showScreen('order-screen');
}
function validateOrder() {
    // Remove 0 qty lines
    for (var i = 0; i < currentOrder.lines.length; i++) {
        if (currentOrder.lines[i].quantity == 0) {
            currentOrder.lines.splice(i, 1);
            i--;
        }
    }
    for (var i = 0; i < currentOrder.lines.length; i++) {
        currentOrder.lines[i].dispOrder = i;
    }
    Pasteque.srv_write(app.conn, Pasteque.Request('TicketsAPI', 'share', 'ticket', currentOrder), validateCallback, errCallback);
    showScreen('restaurant-screen');
}
