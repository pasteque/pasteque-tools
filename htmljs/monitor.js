function resetStatus() {
    document.getElementById('results').innerHTML = "";
}
function addStatus(host, msg, className) {
    var res = document.getElementById('results');
    var newRes = document.createElement('li');
    newRes.classList.add(className);
    newRes.innerHTML = host + ': ' + msg;
    res.appendChild(newRes);
}

function monitor(servers) {
    var monitOk = function(host, data) {
        return function(data) {
            addStatus(host, data['status'], 'ok');
        };
    };
    var monitErr = function(host, req, status, err) {
        return function(req, status, err) {
            if (err == '') {
                err = "No data";
            }
            addStatus(host, 'NOK ' + err, 'nok');
        }
    }
    var srv = [];
    for (var i = 0; i < servers.length; i++) {
        srv.push(Pasteque.Connection(servers[i]['host'], servers[i]['user'], servers[i]['password']));
        var target = null;
        if (servers[i]['level'] < 7) {
            target = Pasteque.srv_oldread;
        } else {
            target = Pasteque.srv_read;
        }
	target(srv[i], Pasteque.Request('TaxesAPI', 'getAll'), monitOk(srv[i]['host']), monitErr(srv[i]['host']));
    }
}
