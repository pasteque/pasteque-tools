#!/bin/bash
#############################################################################
# install.sh - pasteque-backup,   MySQL and PostgeSQL support
# 15/09/2017
# Philippe.corbes@gmail.com
#############################################################################

TARGET="pasteque-backup";

CHK_DIR_EXISTS="test -d";
COPY="cp -f";
COPY_FILE="${COPY}";
COPY_DIR="${COPY} -r";
DEL_DIR="rmdir";
DEL_FILE="rm -f";
INSTALL_FILE="install -m 644 -p";
INSTALL_DIR="${COPY_DIR}";
INSTALL_BIN="install -m 755 -p";
INSTALL_ETC="install -m 644 -p";
INSTALL_CRON="install -m 644 -p";
MKDIR="mkdir -p";

[ "#$PREFIX#" == "##" ] && PREFIX="/usr";
[ "#$DESTDIR#" == "##" ] && DESTDIR="";

${INSTALL_BIN} "${TARGET}-mysql.sh" "${DESTDIR}${PREFIX}/sbin/${TARGET}-mysql";
${INSTALL_BIN} "${TARGET}-postgresql.sh" "${DESTDIR}${PREFIX}/sbin/${TARGET}-postgresql";
${INSTALL_ETC} "${TARGET}.conf" "${DESTDIR}/etc/default/";
${INSTALL_ETC} "${TARGET}.cnf" "${DESTDIR}/etc/default/";
${INSTALL_ETC} "${TARGET}-do-after" "${DESTDIR}/etc/default/";
${INSTALL_CRON} "${TARGET}.cron" "${DESTDIR}/etc/cron.d/${TARGET}";
printf "\n";
printf "Edit configuration files:\n";
printf "	${DESTDIR}/etc/default/${TARGET}.conf and save it to ${DESTDIR}/etc/pasteque/${TARGET}.conf\n";
printf "	${DESTDIR}/etc/default/${TARGET}.cnf and save it to ${DESTDIR}/etc/pasteque/${TARGET}.cnf\n";
printf "	${DESTDIR}/etc/default/${TARGET}-do-after and save it to ${DESTDIR}/etc/pasteque/${TARGET}-do-after\n";
printf "	${DESTDIR}/etc/cron.d/${TARGET} is executed dayly at 23h59. Edit if you want to adjust squeduled calls.\n";
printf "Secure passwords files\n";
printf "	chmod 600 ${DESTDIR}/etc/pasteque/${TARGET}.conf\n";
printf "	chmod 600 ${DESTDIR}/etc/pasteque/${TARGET}.cnf\n";
printf "	chmod 600 ~/.pgpass\n";
printf "\n";
