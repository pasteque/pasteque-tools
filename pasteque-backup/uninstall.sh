#!/bin/bash
#############################################################################
# uninstall.sh - pasteque-backup,   MySQL and PostgeSQL support
# 27/08/2017
# Philippe.corbes@gmail.com
#############################################################################

TARGET="pasteque-backup";

DEL_DIR="rmdir";
DEL_FILE="rm -f";

[ "#$PREFIX#" == "##" ] && PREFIX="/usr";
[ "#$DESTDIR#" == "##" ] && DESTDIR="";

${DEL_FILE} "${DESTDIR}${PREFIX}/sbin/${TARGET}-mysql";
${DEL_FILE} "${DESTDIR}${PREFIX}/sbin/${TARGET}-postgresql";

