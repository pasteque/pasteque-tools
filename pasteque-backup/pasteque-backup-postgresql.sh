#!/bin/bash
# Backup des bases de données PostgreSQL journalier et mensuel avec historique sur une semaine
# 19 sauvegardes au maximum
# Philippe Corbes, le 27/08/2017, Version 1.0
#
# Exemple de fichier de configuration: /etc/pasteque-backup.conf
# #/etc/pasteque-backup.conf
#
# HOST=localhost
#
# WORKINGDIR="/tmp"
#
# BACKUPDIR="/var/backup/db"
#
# OWNER="root:root";
#
# # Spécifier la ou les bases à sauver ou laisser vide pour sauver toutes les bases
# #DATABASES="pasteque wordpress"
# DATABASES=""
#
# # Commande exécutée après la sauvegarde. 
# #DOAFTERBACKUP="/etc/pasteque-backup-do-after"
#
# Stocker les informations de connection dans le fichier /root/.pgpass
##nom_hote:port:database:nomutilisateur:motdepasse 
#*:*:*:<user>:<password>
#
. /etc/pasteque/pasteque-backup.conf

[ "#$HOST#" == "##" ] && HOST=localhost;
[ "#$OWNER#" == "##" ] && OWNER="root:root";
[ "#$WORKINGDIR#" == "##" ] && WORKINGDIR=/tmp;
[ "#$BACKUPDIR#" == "##" ] && BACKUPDIR=$(pwd);
[ "#$DATABASES#" == "##" ] && DATABASES="`LANG=C psql $HOST -l -A -F: | sed -ne "/:/ { /Name:Owner/d; /template0/d; s/:.*$//; p }"`";

NOW=`date '+%Y-%m-%d-%Hh%M'`
DAYW=$(date +"%u")
MONTH=$(date +"%m")

[ ! -d $WORKINGDIR ] && mkdir -p $WORKINGDIR;
[ ! -d $BACKUPDIR ] && mkdir -p $BACKUPDIR && chown $OWNER $BACKUPDIR;

[ "#$DATABASES#" != "##" ] && \
[ -d $WORKINGDIR ] && \
[ -d $BACKUPDIR ] && \
for DATABASE in $DATABASES; do
	BACKUPFILE=$WORKINGDIR/postgresql-$DATABASE.$NOW.sql;
	pg_dump \
		--host=$HOST \
		--no-password \
		--clean \
		--create \
		--format=custom \
		--blobs \
		--encoding=utf8 \
		--quote-all-identifiers \
		--file=$BACKUPFILE \
		$DATABASE;
	
	size=$( wc -c "$BACKUPFILE" | awk '{print $1}' )
	if [ $size -gt 0 ]; then 
		chown $OWNER $BACKUPFILE;
		gzip $BACKUPFILE;
		chown $OWNER $BACKUPFILE.gz;

		# Remplace la sauvegarde journalière
		rm -f $BACKUPDIR/postgresql-$DATABASE.*.sql.$DAYW.gz;
		mv $BACKUPFILE.gz $BACKUPDIR/postgresql-$DATABASE.$NOW.sql.$DAYW.gz;
		chown $OWNER $BACKUPDIR/postgresql-$DATABASE.$NOW.sql.$DAYW.gz;

		# Remplace la sauvegarde mensuelle le premier jour du mois
		if [ "$(date +"%d")" == "01" ]; then
			rm -f $BACKUPDIR/postgresql-$DATABASE.*.sql.M$MONTH.gz;
			cp $BACKUPDIR/postgresql-$DATABASE.$NOW.sql.$DAYW.gz $BACKUPDIR/postgresql-$DATABASE.$NOW.sql.M$MONTH.gz;
			chown $OWNER $BACKUPDIR/postgresql-$DATABASE.$NOW.sql.M$MONTH.gz;
		fi
	else
		echo "Warning: Empty backup file: postgresql-$DATABASE.$NOW.sql !";
	fi

done;

#Synchronisation distante des sauvegardess
if [[ "${DOAFTERBACKUP}" ]]; then
	source ${DOAFTERBACKUP}
fi

