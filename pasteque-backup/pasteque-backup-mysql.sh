#!/bin/bash
# Backup des bases de données MySQL journalier et mensuel avec historique sur une semaine
# 19 sauvegardes au maximum
# Philippe Corbes, le 27/08/2017, Version 1.0
#
# Exemple de fichier de configuration: /etc/pasteque-backup.conf
# #/etc/pasteque-backup.conf
#
# HOST=localhost
#
# WORKINGDIR="/tmp"
#
# BACKUPDIR="/var/backup/db"
#
# OWNER="root:root";
#
# # Spécifier la ou les bases à sauver ou laisser vide pour sauver toutes les bases
# #DATABASES="pasteque wordpress"
# DATABASES=""
#
# # Commande exécutée après la sauvegarde. 
# #DOAFTERBACKUP="/etc/pasteque/pasteque-backup-do-after"
#
. /etc/pasteque/pasteque-backup.conf

[ "#$HOST#" == "##" ] && HOST=localhost;
[ "#$OWNER#" == "##" ] && OWNER="root:root";
[ "#$WORKINGDIR#" == "##" ] && WORKINGDIR=/tmp;
[ "#$BACKUPDIR#" == "##" ] && BACKUPDIR=$(pwd);
[ "#$DATABASES#" == "##" ] && DATABASES="$(mysql --defaults-file=/etc/pasteque/pasteque-backup.cnf -h$HOST --execute='show databases;'|grep -v '^Database$'|grep -v '^mysql$'|grep -v '^performance_schema$'|grep -v ^information_schema$| tr \\\r\\\n ,\ )";

NOW=`date '+%Y-%m-%d-%Hh%M'`
DAYW=$(date +"%u")
MONTH=$(date +"%m")

[ ! -d $WORKINGDIR ] && mkdir -p $WORKINGDIR;
[ ! -d $BACKUPDIR ] && mkdir -p $BACKUPDIR && chown $OWNER $BACKUPDIR;

[ "#$DATABASES#" != "##" ] && \
[ -d $WORKINGDIR ] && \
[ -d $BACKUPDIR ] && \
for DATABASE in $DATABASES; do
	BACKUPFILE=$WORKINGDIR/mysql-$DATABASE.$NOW.sql;
	mysqldump \
		--defaults-file=/etc/pasteque/pasteque-backup.cnf \
		--host=$HOST \
		--databases $DATABASE \
		--opt \
		--add-drop-database \
		--complete-insert \
		--single-transaction \
		--triggers \
		--routines \
		--events \
		--quote-names \
		--hex-blob \
		--default-character-set=utf8 \
		--result-file=$BACKUPFILE;
	
	size=$( wc -c "$BACKUPFILE" | awk '{print $1}' )
	if [ $size -gt 0 ]; then 
		chown $OWNER $BACKUPFILE;
		gzip $BACKUPFILE;
		chown $OWNER $BACKUPFILE.gz;

		# Remplace la sauvegarde journalière
		rm -f $BACKUPDIR/mysql-$DATABASE.*.sql.$DAYW.gz;
		mv $BACKUPFILE.gz $BACKUPDIR/mysql-$DATABASE.$NOW.sql.$DAYW.gz;
		chown $OWNER $BACKUPDIR/mysql-$DATABASE.$NOW.sql.$DAYW.gz;

		# Remplace la sauvegarde mensuelle le premier jour du mois
		if [ "$(date +"%d")" == "01" ]; then
			rm -f $BACKUPDIR/mysql-$DATABASE.*.sql.M$MONTH.gz;
			cp $BACKUPDIR/mysql-$DATABASE.$NOW.sql.$DAYW.gz $BACKUPDIR/mysql-$DATABASE.$NOW.sql.M$MONTH.gz;
			chown $OWNER $BACKUPDIR/mysql-$DATABASE.$NOW.sql.M$MONTH.gz;
		fi
	else
		echo "Warning: Empty backup file: mysql-$DATABASE.$NOW.sql !";
	fi

done;

#Synchronisation distante des sauvegardess
if [[ "${DOAFTERBACKUP}" ]]; then
	source ${DOAFTERBACKUP}
fi

