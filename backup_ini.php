<?php
// Public domain

// Add archive=true to your db.ini file to exclude the db from backup
$dir = '<your *_db.ini directory>/';
$bkpDir = '<your backup directory>/';
$date = date('ymd');
$files = scandir($dir);
foreach ($files as $file) {
	if (substr($file, -7) == '_db.ini') {
		$data = parse_ini_file($dir . $file);
		if (array_key_exists('archive', $data) && $data['archive']) {
			continue;
		}
		$bkpName = $bkpDir . $data['name'] . '-' . $date . '.sql.bz2';
		if ($data['type'] == 'mysql') {
			print('mysqldump -u ' . $data['user'] . ' --password="' . $data['password'] . '" ' . $data['name'] . ' | bzip2 > ' . $bkpName . "\n");
		} else {
			// TODO handle postgresql dump
		}
	}
}
